# Extension and Evaluation of Trajectory Planning Approaches for UAV based Object Inspection and Reconstruction
<i>Shreyas Manjunath</i>
***
In recent years, the use and importance of unmanned aerial vehicles (UAVs) in different markets, such as aerial video and photography, precision farming, security monitoring and disaster relief, as well as 3D reconstruction and mapping has greatly increased. Especially when it comes to the task of inspection, monitoring or mapping, an efficient flight path is of great importance, in order to minimize the flight time and, in turn, saving resources, while at the same time ensuring a full coverage of the area or object of interest. The planning of such an optimal flight path is a very challenging task and of great interest to the scientific community as well as to the end-users.

The aim of this master thesis is

- to investigate the functionalities and usability of the [Structural-Inpsection-Planner (SIP)](https://github.com/ethz-asl/StructuralInspectionPlanner), a well-known an open-source tool for the planning of flight-paths for instruction purposes.
- to extend the SIP to handle additional constraints, such as no-fly zones or obstacles, as well as account for instrinsic properties of the sensor, such as field-of-view or resolution.
- to quantitatively evaluate the computed flight-path in a simulated environment (e.g. Gazebo), as well as to perform a qualitative assessment in a real-world scenario. In both cases the mission planning functionality of a commercial tool, i.e. Agisoft Metashape, should be used as a reference.

## Dates
  <b>Duration: 15.07.2021 - 14.01.2021</b>
  
  <b>Mid-term Presentation:</b>
  
  <b>Final Presentation:</b>
  
## Examination Board / Supervisors
  <b>Examinating Professor: Prof. Dr. Stephan Neser (Hochschule Darmstadt</b>         
  
  <b>Supervisors:</b> Boitumelo Ruf, M.Sc. \& Raphael Hagmanns, M.Sc.


