# LaTeX template for bachelor and master theses
This repository contains a LaTeX template for bachelor and master theses. After cloning change the content of the `README.md` file. It should contain the title of your thesis and your abstract. Furthermore, at the end of your thesis it should contain a section showing how to use the code you've produced during your thesis.

## Table of Contents
3. [Presentation slides](https://gitlab-ext.iosb.fraunhofer.de/goldat/thesistemplate#presentation-slides)
4. [Thesis](https://gitlab-ext.iosb.fraunhofer.de/goldat/thesistemplate#thesis)
5. [Literature](https://gitlab-ext.iosb.fraunhofer.de/goldat/thesistemplate#literature)
6. [Some tips and useful applications](https://gitlab-ext.iosb.fraunhofer.de/goldat/thesistemplate#some-tips-and-useful-applications)

![](cover_page_example.PNG)

## Presentation slides
All presentations held during your thesis should be available here, including all original images and visualizations. When working with Microsoft PowerPoint always create an additional pdf file. Keep in mind the following naming convention.
```
{yyyy_mm_dd}_{zwischenvortrag, abschlussvortrag}_{name of your thesis}.{pptx, pdf}
```
For further information see [presentation folder](https://gitlab-ext.iosb.fraunhofer.de/goldat/thesistemplate/blob/master/presentations/information.md).

## Thesis
This folder contains your LaTeX code. When pushed to the remote repository, gitlab will automatically create a pdf file of your LaTeX code.

## Literature
This folder contains all related work.
```
{year}_{last name of first author}_{title}_{conference_titel_abbreviation}.pdf

Example:
2017_Cinelli_Anomaly_Detection_in_Surveillance_Videos_using_Deep_Residual_Networks_Diss.pdf
```
For further information see [literature folder](https://gitlab-ext.iosb.fraunhofer.de/goldat/thesistemplate/blob/master/literature/information.md).

## Some tips and useful applications
* If possible, use **vector graphics** within your work. E. g. generated plots from Python can easily be saved as vector graphics.
* For graphs and different visualizations with a graph like structure, take a look at [yEd](https://www.yworks.com/downloads#yEd).
* For simple class diagrams take a look at [UMLet](http://www.umlet.com/).
* Another useful tool is the [.gitignore file generator](https://www.gitignore.io/). Add your used software and it generates automatically a .gitignore-File.
* Some useful hints for [training deep neural networks](https://arxiv.org/pdf/1803.09820v1.pdf)
