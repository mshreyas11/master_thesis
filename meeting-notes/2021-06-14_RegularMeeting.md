# Regular Meeting
<i>2021-06-14</i>

## Simulators
- <b>Px4 Firmware in Gazebo: https://docs.px4.io/master/en/simulation/gazebo.html</b>
- Flightmare (based on Airsim) https://github.com/uzh-rpg/flightmare
- FlightGoggles https://github.com/mit-aera/FlightGoggles

Insights:
- Px4 Simulator simple and easy to start-with simulator --> also allows 3D Reconstructions.
- Filghtmare allows PBR since based on UE --> appropriate for 3D Reconstructions

## Next Steps
- Build up a pipeline with SIP and Px4 Simulator, which allows
    - to plan a trajactory based on a model in SIP
    - simulate and visualize the flight in Px4 Simulator
    - capture the simulated trajectory and camera data from the simulator
- Copy the SIP repository into a new repo on gitlab-ext
    - Clone from github and push to gitlab-ext


