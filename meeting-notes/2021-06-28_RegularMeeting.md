# Regular Meeting
<i>2021-06-28</i>

- Pipeline with SIP, Gazebo and Px4-QGroundControl is steup and ready to use
- Able to plan a trajectory, fly the drone and capturing camera images
    - If too many waypoints, the drone doesn't fly over all waypoints but just passes them as fly-by
- Model of CAD model of ROBDEKON is built in Blender
- Node for creating ENU points from GPS coordinates


## Next Steps
- use MAVRos Offboard API in Mission mode, instead of QGroundControl --> Link from Raphael ( https://docs.px4.io/master/en/ros/mavros_offboard.html)
    - Stand alone package
- Check how SIP works with the CAD model of ROBDEKON
    - Are the Triangles small enough?
    - What happens with the holes?
- Test Pipeline with CAD Model of ROBDEKON
- What about the parameters of the camera?
    - Field-of-View
    - Roll-Pitch-Yaw
    - Image-Resolution
    - ...
- Copy the SIP repository into a new repo on gitlab-ext
    - Clone from github and push to gitlab-ext
- Contact Prof. Neser about regularities, like
    - Mid-Term presentation
    - Regular status updates



