# Regular Meeting
<i>2021-07-12</i>

- flying the drone in offboard mode (more complex), keep in mind to use mission mode (simpler)
    - testing waypoints in mission is more simpler
- how SIP works with the CAD model of ROBDEKON
    - Triangel-Size: The triangle needs to be fully inside the FoV of the camera
        --> The resulting resolution is dependent on the trianlge size
- What parameters of the camera can be set to SIP?
    - Field-of-View
    - Roll-Pitch-Yaw
- Contact Prof. Neser about regularities, like
    - Mid-Term presentation: yes
- Registgration in progress
    - Thesis Title: Extension and Evaluation of Trajectory Planning Approaches for UAV based Object Inspection and Reconstruction
- New Dataset available:
    - Paper: https://www.int-arch-photogramm-remote-sens-spatial-inf-sci.net/XLIII-B1-2021/157/2021/
    - Website: https://www.uni-weimar.de/en/media/chairs/computer-science-department/computer-vision/research/bauhaus-path-planning-challenge/


## Next Steps

- Implementing the connection between SIP and Offboard-Control
- Put a Texture on the ROBDEKON model
    - provide 2-3 different types of textures in order to evalute
- Read the paper on the bauhuas path planning challenge
- Presentation / Overview on the Pipeline and Worklfow of SIP
    - In order to analyze where to plug-in the extensions
- Tasks for the Thesis
    - Extension: Desired Resolution (GSD)
       - Check on how the SIP is planning the trajectory w.r.t the triangle size
            - Does the triangle always cover the whole view frustum?
            - Is the viewpoint connected to a specific triangle?
    - Extension: No-Fly-Zones / Obstacles
        - How to add the Zones / obstacles to the optimizer or planner
    - How to tackle dynamic obstacles
    - Runtime analysis






