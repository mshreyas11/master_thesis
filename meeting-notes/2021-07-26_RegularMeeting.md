# Regular Meeting
<i>2021-07-26</i>

- Created textures for ROBDEKOM CAD model
- Thesis registered (Deadline 14.01.2022)
- Flying the drone in offboard mode (more complex) or mission mode (simpler)
    - subscribing to SIP by PoseArray topic
- Overview on the Pipeline and Worklfow of SIP created
    - Insight: No-Fly-Zones / Obstacles is already realized 
    - Insight: the view point sampling doesn't seem intuitve

## Next Steps

- Create a very simple scenario: A simple wall to expect
- Colorize the triangles dependent on whether they are cover or rejected
- Analyze the results of the different Optimizations
    - 1. tie together the camera feed with the computed view points into a bagfile, in order to analyze how the triangles are covered by the sensor afterwards
    - 2. use mission planner of QGroundControl to compute a basic view point list and pass it directly to TSP solver








