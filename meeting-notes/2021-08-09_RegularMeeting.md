# Regular Meeting
<i>2021-08-09</i>

- Create a very simple scenario: A simple wall to expect
    - This works well but only one iteration of first AGP+TSP
    - The settings work well with the Robdekon building as well. 
- Added visualization
    - Rejected Triangles
    - Pose, VPs and Arrows
- Insights:
    - d_min and d_max are included in the QP as constraints
    - setting points manually for TSP optimization is possible 
    - more iterations lead to difficult trajectory
    - AGP and consecutive TSP optimization is interconnected
    - d_min and d_max are not always respected
    - Setting dmax-dmin to a very small range works as well.
- Merge to single executable launch file
    - done 

## Next Steps
- Look deeper into dualBarrierSampling function from rotorcraft.hpp
    - track triangle coverage constraint through this function
    - replace this function with a typed constraint system in a different module
    - turn constraints in this function on and off, see how this infects the results
- Seperate TSP solver, run only one iteration of AGP and make TSP (and RRT) a subsequent step
- Create robdekon hall in different triangle tessellations
- Analyze the results of the different Optimizations
    - 1. tie together the camera feed with the computed view points (or center view point, if easier) into a bagfile, in order to analyze how the triangles are covered by the sensor afterwards
- Test the Agisoft Planner

(idea: uncertainty map for triangle mesh, https://github.com/ethz-asl/polygon_coverage_planning)




