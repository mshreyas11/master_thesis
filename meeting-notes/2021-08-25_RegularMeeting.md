# Regular Meeting
<i>2021-08-25</i>

- Created robdekon halle in different triangle tessellations.
- Separated TSP and ran AGP for 1 iteration. 
- Analyzed the complete AGP constraints and Quadratic problem inputs. 
- Removed dmin and dmax constraints and ran the SIP with only 7 constraints.
	- Initial Iteration of AGP results the same as with (dmin, dmax) constraints. Subsequent iterations provide some of the VPs close by to the object  compared to the VP position with (dmin,dmax) constraints applied. 
- Publish VP-Triangle pair list.
- Custom method was re-written to replace existing dualbarriersampling method to make it more flexible.

## Next Steps
- Capture image data from the camera of the drone
- Run a reconstruction with COLMAP (https://colmap.github.io/tutorial.html)
- Test the Agisoft Planner
- Focus on task to specify desired Resolution/GSD
    - Calculate d_min and d_max given the desired resolution and the camera parameters
    - Analyze whether d_min, d_max, and, in turn, the desired resolution are met for each view-point
    - if not, why are they not met?
- Check the data on the Bauhaus Challange: http://path.medien.uni-weimar.de/#participation
