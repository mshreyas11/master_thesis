# Regular Meeting
<i>2021-09-06</i>

- Capture image data from the camera of the drone
- Run a reconstruction with COLMAP (https://colmap.github.io/tutorial.html)
- Check the data on the Bauhaus Challange: http://path.medien.uni-weimar.de/#participation
    - Not in a suitbale format to be directly be used in SIP, i.e. obj file
- Exported Sampling of view-points into seperate Class
    - possibility to adjust viewpoint sampling more easily and flexible


## Next Steps
- Test the Agisoft Planner
- Further improve setting of constraints for Viewpoint sampling
    - Split function into position and orientation
    - start of by add high-level function to current constraints
    - add more high-level "Activation" functions, like activateDMinConstraint(bool)
    - add more high-level "Setter" functions, like setDMinConstraint(value)
- add a simple evaluation setting with only a few trianlge, to see the change on a single triangle

- Focus on task to specify desired Resolution/GSD
    - Calculate d_min and d_max given the desired resolution and the camera parameters
    - Analyze whether d_min, d_max, and, in turn, the desired resolution are met for each view-point
    - if not, why are they not met?

