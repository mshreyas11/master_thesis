# Regular Meeting
<i>2021-09-22</i>

- In the fixed-wing extension additionally constraints ensure a more smooth trajectory between consecutive viewpoints, since the fixed-wing aircraft cannot move as swiftly as the rotor-based aircraft.
- Generalize the optimization and constraints to handle arbitrary number of corner points
    - new class / datastructure, Triangle -> Polygon
- Read into "hotstart"


## Next Steps
- Start a new code base with only the parts that are needed for a rotor-craft and generalize it to work with polygons instead of triangles
- Read into related work and see whether something helpful can be deduced from them
