# Regular Meeting
<i>2021-10-04</i>

- Good progress on pipline with new code base
- Tested new implementation with one and multiple triangles


## Next Steps
- Integrate TSP solver from Google instead the one from SIP
- Test with polygons
