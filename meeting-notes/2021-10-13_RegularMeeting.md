# Regular Meeting
<i>2021-10-13</i>

- Completed pipline with new code base (using TSP from google)
- Tested new implementation with one and multiple triangles
- Tested with Robdekon model

## Next Steps
- Test with polygons (on similar example with the individual triangles)
- Check on the accuracy measures of the bauhaus challenge
- Mid-Term presentation: 28.10. at 15:00
- Prepare a time-line for the last weaks of the thesis and the open TODOs
