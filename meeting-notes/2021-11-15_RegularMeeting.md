# Regular Meeting
<i>2021-11-15</i>

- Viewpoint sampling with polygons is working

## Goals
- Path-planning for UAV on actual model and compare against Metashape
- Final demonstration on Excevator
- Quantitative comparison on Modells of Bauhaus challenge

## Evaluation Metrics
- Pathlength
- Yaw-Rate
- Verify the number of Triangles that are convered with desired GSD
(- Baseline between views to triangle)

## TODO
- [ ] Finalize Polygon Implementation
  - [x] Publish
  - [x] Centroid as reference point
  - [ ] Maximize Area inside FoV (suspected to finish by 15.11.)
  - [x] Confirm Correct AGP
- [ ] Module for preprocessing triangles into polygons

( - [ ] Compute path with metashape and convert into KLM)
