# Regular Meeting
<i>2021-11-29</i>

- Implementing simple algorithm to convert triangles into polygons until 30.11.

## Evaluation Metrics
- Pathlength
- Yaw-Rate
- Mean number of viewpoints for each triangle
- Verify the number of Triangles that are convered with desired GSD

## Evaluation Data

- Wall of Triangles
- Robdekon
- Bauhaus Bridge-Pillar
- Bauhuas Artificial Building
