\chapter{Technical Background}
\label{tb}
This chapter provides brief explanation on different tools, technologies,  important topics, terminologies and the UAV used for this master thesis. An essential part of this chapter supports clear immersion and understanding the thesis.

\section{Unmanned Aerial Vehicles}
\label{tb:fundamentals:UAV}
Unmanned Aerial Vehicles (\acrshort{uav}) are defined as flying aerial robots that can fly autonomously or be controlled by a ground control station by a human operator. UAVs are generally termed as drones. But the term "drone" can be used for any autonomous or semi-autonomous ground, air, sea level or underwater robot. Therefore, UAVs can be termed as "aerial drones". A typical UAV is equipped with the sensors such as barometer, GPS, accelerometer, gyroscope and dedicated obstacle avoidance module for the basic functioning of the UAV. Cameras and other external peripheral attachments can be mounted to the UAV as a payload depending on the vehicle's feature and capacity. % Add Images
A setup containing the UAV, Ground Control Station equipment and the human operator is collectively known as Unmanned Aircraft System (UAS). The UAS is generally classified as Open, Special and Certified groups based on the weight of the aircraft and applications and further they are divided into sub-classes depending on the range of operation and usability scenario. Under the commercial UAVs, the vehicles are classified as: 
\begin{itemize}
	\item Nano UAV -- Under 250 g in weight
	\item Micro Aerial Vehicles (MAV) -- 250 g to 2 Kg in weight
	\item Small UAV (sUAV) -- 2 Kg to 25 Kg in weight
\end{itemize}
UAVs find their application and usage in many sectors such as remote sensing, agriculture, military, surveillance, civil field surveys, disaster management and search-rescue operations, research and development, recreational usage, cinematography, structural inspection to name a few.  

\subsubsection*{Rotorcraft Model}
\label{tb:fundamentals:UAV:rotorcraft}
Rotorcraft model is a type of UAV that has one or more downward thrusting propellers and take off vertically from the ground. They are agile and can hover with ease. A quadrotor or quadcopter (4 rotor), hexacopter (6 rotor) and Octocopter (8 rotor) are some of the popular rotorcraft models. The other types of UAVs classified based on design are the Fixed-Wing, Single-Rotor Helicopter and Hybrid VTOL models. 

\begin{figure}[h]
	\centering
	\subfloat[DJI Matrice 210 RTK v2.]{\includegraphics[width=0.49\linewidth]{images/DJIdrone.png}}
	\subfloat[DJI Agras MG-1P Octocopter.]{\includegraphics[width=0.49\linewidth]{images/djiocto.jpg}}
	\caption{Examples for a rotorcraft model. Source: \citeauthor{dji_2021} \cite{dji_2021}.}
	\label{rotorcraftimages}
\end{figure} 

The \cref{rotorcraftimages} shows the images of the two popular rotorcraft models developed by DJI. The DJI Matrice 210 RTK v2 is a \acrshort{uav} used for industrial applications, representing as an example for a quadcopter and the DJI Agras MG-1P is a \acrshort{uav} used in agricultural applications, representing as an example for an octocopter. 

\section{Fundamentals}
\label{tb:fundamentals}

\subsection{Viewpoint Trajectory}
\label{tb:fundamentals:VPtraj}

\subsubsection*{Viewpoint}
\label{tb:fundamentals:VPtraj:VP}
Viewpoint is any position or point in 3D space from which a meaningful inspection can be performed on the interested area or object using an inspection equipment. In the context of structural inspection using UAVs, the inspection equipment is the UAV and it can inspect a particular region of the building or the object. Mathematically, a viewpoint is an ordered collection of position and orientation data, for example: 

\begin{equation}
	VP_{i} = [x, y, z, \text{roll}, \text{pitch}, \text{yaw}]
\end{equation}

To every view point, there exists a position [x, y, z] and orientation [roll, pitch, yaw]. For simplifying the orientation, only yaw angle is considered in the work of master thesis. The detailed explanation on orientation consideration with respect to the type of vehicle chosen is explained in the further chapters.

\begin{figure}[h]
	\centering
	\includegraphics[width=6cm, height=6cm]{images/Fundamentals-VP-image.png}
	\caption{Depiction of a viewpoint and viewpoint trajectory.}
	\label{tb:vptrajimage}
\end{figure}

\subsubsection*{Trajectory}
\label{tb:fundamentals:VPtraj:traj}
A viewpoint trajectory is a curve constructed by joining all the viewpoints. Thus, a trajectory is a list of all the viewpoints that defines the meaningful path around the object of interest. The viewpoints are smartly ordered into a list making up an optimal trajectory or tour for the UAV to fly around as formulated in the \cref{traj_eq}.
\begin{equation}
	\label{traj_eq}
	\text{Traj}_{VP} = \{VP_1, VP_2,...., VP_n\}
\end{equation}
The \Cref{tb:vptrajimage} shows the depiction of group of viewpoints and their trajectory around an object. The cube at the center represent any area under the inspection. The green dots are the respective viewpoints that enable the inspection and the curve joining all the viewpoints represents the trajectory.

\subsection{Art Gallery Problem}
\label{tb:fundamentals:AGP}
The art gallery problem (\acrshort{agp}) is a computational geometry problem of visibility. It originates from the task of efficiently placing watch guardians around an art gallery or museum in order to achieve a full coverage of the area . In recent times, watch guardians are replaced by cameras. Therefore, the \acrshort{agp} can be defined as finding minimum number of cameras required together with their optimal positions, in order to monitor the whole art gallery.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{images/AGPExplanation.pdf}
	\caption{Depiction of art gallery and camera placement in 2D. Source: \citeauthor{kindermann_2020} \cite{kindermann_2020}.}
	\label{agpexplanation}
\end{figure}

The \cref{agpexplanation} presents the overview of solving the AGP in 2D space. The space $P$ represents the art gallery. Any position in the art gallery can be chosen to place a camera $c$ with its coverage area represented by the yellow region in the figure. Any given point $q$ in this region is covered by the camera. Geometrically, the camera $c$ and an arbitrary point $q$ in the coverage region forms a line segment $\overline{cq}$. The cameras are smartly placed inside the art gallery to cover the complete region of the gallery with cameras that are represented in the red dots in the figure. The concept of \acrshort{agp} is used in the development of the \acrshort{sip} as well as for the thesis.  

\subsection{Traveling Salesman Problem}
\label{tb:fundamentals:TSP}
Traveling Salesman Problem or in short \acrshort{tsp} is a real world inspired computational optimization problem. It is NP-Hard in nature. It is the task of determining the shortest or quickest and most efficient tour for a salesperson to travel with a provided list of destinations. TSP has led to many innovations in problem solving and has applications in planning, logistics and DNA sequencing.

\begin{figure}[h]
	\centering
	\includegraphics[width=6cm, height=5cm]{images/TSP_Wiki.png}
	\caption{Solution illustration of TSP. Source: \cite{wiki:Travelling_salesman_problem}.}
\end{figure} 

There are different types of solutions available, namely: Greedy algorithm, Nearest Neighbour, Lin Kernighan Heuristic (LKH) \cite{HELSGAUN2000106} and Chained Lin Kernighan Heuristic \cite{Applegate2003ChainedLF}. The \acrshort{tsp} concept is used for the purpose of smartly arranging the sampled viewpoints in \acrshort{sip} to generate a viewpoint trajectory as a part in the development of the thesis.


\section{Quadratic Programming}
\label{tb:QP}
Quadratic programming is a methodology for solving non-linear mathematical optimization problems. The solving strategy used in most of the implementations is a model-based approach. Generally, a quadratic problem needs to be solved either to maximize or minimize the variable of focus, while at the same time satisfying linear constraints. There are many different types of solution methods available to solve a quadratic programming problem, such as interior point method, active set strategy, augmented lagrangian, gradient projection to name a few. The use of quadratic programming is dominantly found in scientific research. Although the concept of \acrshort{qp} has also many applications in the field of finance, economics \cite{MCCARL197743} and production operations. \acrshort{qp} is also been proved to be an effective mathematical tool in geometric problems such as convex or non-convex optimizations as shown by \citeauthor{schoenherr02QP} \cite{schoenherr02QP}.

The standard form of a quadratic programming problem is given by:
\begin{equation}
\begin{aligned}
	\min_{\mX} \quad & \frac{1}{2} \mX^{T} H \mX + \mX^{T} \vd(\omega_0)\\
	\textrm{s.t.} \quad  & lbA(\omega_0) \leq \mA \mX \leq ubA(\omega_0)\\
	\quad & lb(\omega_0) \leq \mX \leq ub(\omega_0)
\end{aligned}
\label{qpstdform}
\end{equation}
 
where \textit{H} is a positive semi-definite symmetric matrix, \textit{X} is a variable or a variable matrix, \textit{d} is a gradient vector dependent on an initial parametric value $\omega_0$, \textit{A} is the constraint matrix, \textit{lbA} and \textit{ubA} are the lower and upperbound constraint values, \textit{lb} and \textit{ub} are the variable lower bound and upper bound values, respectively.

Quadratic functions can be constructed or formulated as a convex quadratic problem or a concave quadratic problem. In this thesis, there is a need of convex optimization for the viewpoint sampling process. \acrshort{sip} makes use of a popular active set strategy distribution called qpOASES \cite{qpOASES,Ferreau2008}. For the purpose of improving the functionality, the work make use of other methods and features of qpOASES in the thesis work.

\section{Robot Operating System}
\label{tb:ROS}
The \acrfull{ros} \cite{quigley2009ros} provides libraries and development tools to facilitate software engineers in the design and development of robot applications. It gives hardware abstraction, low-level device control, device drivers, libraries, visualizers, message-passing etc.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.7\linewidth]{images/ros_node.pdf}
	\caption{Messaging in ROS.}
	\label{ros_node}
\end{figure}

A developed software or application can be a node itself or part of a node, which publishes or subscribes to a topic. A channel created between subscriber and publisher is called a topic and messages are exchanged through channels. A “node" or "group of nodes”, as shown in \cref{ros_node}, can be executed both on remote master PC as well as on the robot. Multiple robots can also use similar way of exchanging data through topics. This type of communication is based on message queuing telemetry transport (MQTT) which is based on a publisher-subscriber architecture. In the recent years, more and more UAVs are using ROS as their middleware. As in the case of robots, ROS simplifies the interfacing of new sensors or payloads on the UAVs with flexible wrapper code. Recent innovations in UAV applications with ROS platform have proven its credibility and extensibility. ROS has been used in this thesis for connecting multiple nodes in the planning and simulations pipeline, controlling the UAV and visualization purpose.     

\section{PX4 Autopilot}
\label{tb:px4autopilot}
PX4 Autopilot, or simply PX4, is an open-source professional autopilot stack for UAVs. The key features of this software is its ease of use, adaptability, and, most crucially, its lightweight. Both \acrshort{hitl} and \acrshort{sitl} scenarios are supported by PX4. It comes with a UAV hardware and software stack that enables developers to scale up development and maintenance. PX4 is also a part of an extensive ecosystem of environments and protocols such as ROS, mavsdk, mavlink, QGroundControl, and Pixhawk, which makes interoperability much easier and scalable. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{images/px4_arch_fc_companion.pdf}
	\caption{Architecture of PX4. Source: \cite{px4_2021}.}
\end{figure}
 

PX4 can be used to connect to simulators or can also be used as a firmware on UAV hardware. Therefore, testing of the developed software can be entirely performed in a simulation environment before deploying it on the real UAV for real-world evaluation and usage. PX4 allows users to control the vehicle and its parameters. An external API can be developed and deployed as per the requirements of the user with different flying modes to choose from, velocity to adhere to, and a list of setpoints to reach with safe takeoff and landing. PX4 supports Gazebo \cite{Aguero-2015-VRC,Koenig-2004-394}, a robotics simulator that is compatible with the ROS environment. 

