\section{Planner-Simulation Pipeline}
\label{Imp:ps-pipeline}
The \acrshort{sip} provides the final viewpoint trajectory as a file and the publisher sub-block (c.f. \cref{Imp:presetup}) developed allows any external node to subscribe to the viewpoint trajectory ROS channel. The off-board control node discussed in the previous \cref{Imp:offbcntl} can be modified to subscribe to the viewpoint trajectory published by the planner node of SIP. This connects the planner, the PX4 stack and the simulator which forms a plan-and-fly architecture.

\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{images/planner-simulationPipeline.pdf}
	\caption{Architecture of planner-simulation pipeline.}
	\label{Imp:ps-pipeline-image}
\end{figure}

The \cref{Imp:ps-pipeline-image} depicts the end-to-end pipeline that connects the planner and the simulator under the \acrshort{ros} environment. The SIP settings file is modified to carry the fields that can be used by the users to control the SIP as well as the off-board control node parameters at one place as shown in the \cref{tab:settingsfileformat}. As mentioned before, the off-board control node is modified to subscribe to the specific ROS channel that provides the viewpoint trajectory data. \acrshort{ros} is used as the middleware underlying the pipeline as well as connecting the planner to the simulator. It offers multiple nodes to be executed using a single launch file. Therefore, a single point of control is enough to plan and fly the UAV.

\begin{table}[]
	\centering
	\begin{tabular}{@{}llcc@{}}
		\toprule[1.5pt]
		& \multicolumn{1}{c}{\textbf{Parameter Name}} & \multicolumn{1}{c}{\textbf{Units}} & \multicolumn{1}{c}{\textbf{Comments}} \\ \midrule
		1 & \textbf{Planner}                            &                                    &                                       \\
		& $d_{\text{min}}$, $d_{\text{max}}$                                  & meter (m)                               &                                       \\
		& Min. incidence angle                         & degree                             &                                       \\ \hline
		2 & \textbf{Rotorcraft}                         &                                    &                                       \\
		& Max. velocity                               & m/s                           &                                       \\
		& Max. angular velocity                       & rad/s                         &                                       \\ \hline
		3 & \textbf{Space boundary}                     &                                    &                                       \\
		& Max. space size                             & meter (m)                          & x, y and z size                       \\ 
		& Space center                                & meter (m)                                   & 3D coordinate {[}x, y, z{]}           \\ \hline
		4 & \textbf{Starting pose}                      &                                    &                                       \\
		& Position                                    & meter (m)                          
		& [x, y, z]                                      \\
		& Orientation                                 & degree                             
		& [roll, pitch, yaw]                                      \\ \midrule
		5 & \textbf{Other fixed poses}                  & meter (m), degrees                 & {[}x, y, z, roll, pitch, yaw{]}       \\ \midrule
		6 & \textbf{Camera}                             &                                    &                                       \\
		& \acrshort{fov}            & degree                             & {[}horizontal, vertical{]}            \\
		& Pitch                                       & degree                             & Pitch angle of the camera\\
		\bottomrule[1.5pt]            
	\end{tabular}
	\caption{Fields in the settings file and their format.}
	\label{tab:settingsfileformat}
\end{table}

The above mentioned pipeline architecture is implemented in such a way that each block represents a stand-alone node in the implementation. The mesh and the parameter data is obtained as an input by the request node that creates a service request for the planner node to use the request data for computing the viewpoint trajectory. The planner node, thus generates the viewpoint trajectory and publishes "\textbackslash computed\_viewpoint\_trajectory" topic\footnote{Topic is the alternate technical name used by the ROS documentation referring to a message channel.} for other nodes to access the computed data. Here, the control node as shown in the \cref{Imp:ps-pipeline-image} subscribes to this topic. The control node is already aware of the name of the topic to subscribe which is mentioned by the user in the settings file at the launch. Furthermore, the control node starts the flight and publish the individual viewpoints as explained in the \cref{Imp:offbcntl}. It can be noticed that the control node acts as a bridge between the planner and the PX4 stack. On one hand, it is a subscriber that seeks data and on the other hand it plays the role of a GCS.

The PX4 stack can be used to connect a simulation environment like gazebo or a real UAV. The stack can be installed on the on-board hardware of supported UAVs \cite{px4_2020}. In addition, the user can develop an evaluation component to compute some of the metrics for qualitative and quantitative assessments. The pipeline is user-friendly and versatile for subsequent extensions with the SIP and the off-board control node, as well as a promising PX4 flight stack that can be adapted to simulators, a real UAV, and other external components as needed. All the existing features of SIP such as the rviz visualization of mesh, the rejected triangles, the viewpoints and the final trajectory as well as the generation of tour and log files remains unchanged after the development of this pipeline.  