\section{Viewpoint Sampling}
\label{Imp:VPSampling}
In \cref{SIP:VPS}, the initial viewpoint sampling routine of \acrshort{sip} is explained in detail. Furthermore, the identified drawbacks of this routine are discussed in \cref{SIP:drawbacks}. A new code base is created to accommodate the new features and structural changes in the code architecture. This section discusses the implementation of a new AGP solver module with the viewpoint sampling concept inspired by the work of \citeauthor{BABOOMS_ICRA_15} (\citeyear{BABOOMS_ICRA_15}) \cite{BABOOMS_ICRA_15}. The SIP's viewpoint sampling block is very inflexible and adding new features or changes is time-consuming. Reiterating the fact, that viewpoint sampling block does not provide dynamic constraint addition facility with its complex and rigid code design. To overcome these drawbacks, a new module is developed which is well orchestrated as well as detached itself from unnecessary inter-link between the sub-nodes.

The \acrshort{sip} supports only triangulated mesh in its sampling process and all the methods supporting the viewpoint sampling are developed to compute properties dedicated to triangles. Although, the input to the viewpoint sampling is triangulated mesh, for the future use, the "Triangle" class is extracted and replaced with a "Polygon" class. This class has all the initialization methods to compute polygonal properties such as area, area normal vector, hyperplane normals whenever an instance is created. The new AGP solver is developed to use a polygonal instance\footnote{Polygon class does not change the way in which a triangle is treated in sampling process, since a triangle is a 3-sided polygon.} as an input and finally sample a viewpoint.

\cref{appendix:newAGPClassDiagram} depicts the \acrshort{uml} diagram of the structure of viewpoint sampling in the new code base. The main thread contains polygon objects that are created based on the mesh input. For every triangle in the mesh, a "Polygon" object is instantiated. Furthermore, for each polygon object, an AGP solver is created for the purpose of viewpoint sampling. AGP solver class contains all the necessary check methods and initialization methods required for constructing the QP matrices. Every method in the class and the class itself are designed in such a way that future enhancements are simple to implement. In addition, the planner node has access to read and write into the viewpoint list. All the sampled viewpoints are stored in this list.

To reduce the design complexity, the viewpoint sampling flow is divided into multiple and individual methods. The dynamic constraint count is facilitated in the new implementation for flexibility. In case of distance range constraint depicted by the \cref{planarconstraintseq2}, the projection of distance vector from first vertex of the triangle ($x_1$) to the possible viewpoint position ($g$) onto the area normal vector ($\hat{a_N}$) is bound by the minimum distance ($d_{\text{min}}$) and the maximum distance ($d_{\text{max}}$). The reference point considered for the distance range calculation as one of the vertices of the triangle certainly biases the viewpoint position towards the selected vertex. Thus, it results in a bad viewing position that in fact influences the viewing angle and the interested triangle from this viewpoint position might result in being rendered close to the edges of the viewing frame. Therefore, in the AGP solver, the centroid of the triangle ($m$) is chosen as the reference point for the distance range constraint. The corresponding equation and the final constraint matrix are as shown in the \cref{planarconstraintseq2_new,finalconstrainteq_new}:

\begin{equation}
	\label{planarconstraintseq2_new}
	\begin{aligned}
		d_{\text{max}} \geq (\vg - \vm)^T\cdot\hat{a}_{N} \geq d_{\text{min}}
	\end{aligned}
\end{equation}

\begin{equation}
	\label{finalconstrainteq_new}
	\begin{aligned}
		\begin{bmatrix}
			\infty\\
			\infty\\
			\infty\\
			d_{\text{max}}\\
			\infty\\
			\infty\\
			\infty\\
			\infty
		\end{bmatrix}
		\geq
		\begin{bmatrix}
			\hat{n}_1^T\\
			\hat{n}_2^T\\
			\hat{n}_3^T\\
			\hat{a_N}^T\\
			\hat{n}_{\text{right}}^T\\
			\hat{n}_{\text{left}}^T\\
			\hat{n}_{\text{lower}}^T\\
			\hat{n}_{\text{upper}}^T
		\end{bmatrix} \cdot g^k
		\geq
		\begin{bmatrix}
			\hat{n}_1^T \cdot \vx_1\\
			\hat{n}_2^T \cdot \vx_2\\
			\hat{n}_3^T \cdot \vx_3\\
			d_{\text{min}} + \hat{a_N}^T \cdot \vm\\
			\hat{n}_{\text{right}}^T \cdot \vm\\
			\hat{n}_{\text{left}}^T \cdot \vm\\
			\hat{n}_{\text{lower}}^T \cdot x_{\text{lower}}^{\text{cam}}\\
			\hat{n}_{\text{upper}}^T \cdot x_{\text{upper}}^{\text{cam}}
		\end{bmatrix}
	\end{aligned}
\end{equation}

In addition, the visibility check method is also modified to facilitate the centroid as the reference point. \cref{SIP:Structure,SIP:TP} mention that the \acrshort{sip} employs iterative sampling method to compute the final viewpoint trajectory. But the \cref{robdekon_iter5,robdekon_iter10} show that the final trajectory is not intuitive and the successive iterations arranges the viewpoint more haphazardly. A mesh similar to \cref{testmeshoutput} is expected to have a considerably simpler and uniform viewpoint arrangement. The initial iteration of the viewpoint sampling process alone provides an easier arrangement of the viewpoints to the corresponding triangles with complete coverage. The viewpoint arrangement appears realistic and intuitive to the user, even though the single iteration scheme does not reduce the path length as effectively as its multiple-iteration counterpart. Therefore, a single-iteration scheme is chosen while developing the AGP solver block in the new code base.

Further, a list of random triangles are subjected to the viewpoint sampling process to check the sanity of the corresponding sampled viewpoints. It is found that the viewpoint position is not sampled in front of the triangle aligned to the centroid. Some triangles are observed from more distant viewpoints and some viewpoints are found to be located closer to the triangle. There is also a possibility of the high influence of the pitch angle on the viewpoint position due to dip relative to the body of the UAV, a positive compensation of the UAV's z-coordinate will allow the full visibility of the triangle inside the camera frame as shown in the \cref{UAVDisplacementForPitch}. In addition, the \acrshort{qp} indirectly considers the pitch angle through the upper ($\hat{n}_{\text{upper}}$) and the lower ($\hat{n}_{\text{lower}}$) normals of the camera frustum.

\begin{figure}[h]
	\centering
	\includegraphics{images/UAVDisplacementForPitch.pdf}
	\caption{Relative compensation in position for the camera pitch angle setting. UAV image source: \cite{3DRIrist12:online}.}
	\label{UAVDisplacementForPitch}
\end{figure}

To address this problem, a subsequent correction of the viewpoint position is necessary. The viewpoint position is sampled with the help of \acrshort{qp} and any further adjustment to the sampled position is said to be meaningful when the correction is performed using the QP.  By using the solution generated by the QP as gradient feedback to a subsequent QP, the viewpoint position can be subjected to correction. The qpOASES software \cite{qpOASES} provides two different methods for solving the QP, namely, the "init" and the "hotstart" methods. The init method is used for solving the first QP and the hotstart method is used for subsequent solving of the QPs. Although the QP equation and the Hessian matrix ($A$) remains the same, the gradient vector ($d$), the lower- ($lbA$) and the upper ($ubA$) bounds, as well as the minimum ($lbX$) and the maximum ($ubX$) position values, are subject to change based on the correction required. The following equations show how the new gradient vector and bounds are calculated.

\begin{equation}
	\label{Imp:newgradienteq}
	\begin{aligned}
		d_{\text{new}} = -2\, \alpha_{w} \, \vg_{\text{init}} - 2\,(\vg_{\text{init}} + \vo_c) - 2\,(\vg_{\text{init}} - \vo_c)
	\end{aligned}
\end{equation}

where offset correction vector, $\vo_c$ is a 3D vector and $\alpha_{w}$ is a weight factor that are provided by the user. The $\vo_c$ acts as a bias factor to choose two nearby positions to the initial position solution $\vg_{\text{init}}$. The gradient vector, as formulated by the \cref{SIP:gradientvectoreq}, is adapted to construct the new gradient equation (\cref{Imp:newgradienteq}) with the help of the offset correction vector $\vo_c$. Additionally, the $lbX$ and the $ubX$ are changed such that they create a cubic space around the $g_{\text{init}}$ for the subsequent QP which acts as a quarantined boundary to work with. After computing the new inputs, the subsequent QP is solved using the hotstart method and the primal solution of the QP is obtained which is considered as the new viewpoint position. The number of viewpoint positions adjusted for the custom mesh (\cref{Imp:robdekon_model}) by subsequent QP solving approach are minimal and the collective impact of this approach on the mesh is insignificant.

The sampled viewpoints generated by the AGP solver are stored into viewpoints list by the planner node as explained earlier. This list is ready to be processed by a trajectory planner to compute an optimal viewpoint trajectory.