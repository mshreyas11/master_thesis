\section{Extension for Polygons}
\label{Imp:XtensionPoly}
The triangulated mesh type is very popular and widely used in the computer graphics.  Quad-mesh type is also one of the meshing formats that uses quadrilaterals for geometric modeling and has further sub-categories. There are some meshes that contain majority of quadrilaterals facets and a small minority of triangle facets. They are termed as quad-dominant hybrid meshes. The quad-meshes have their own advantages in many applications, such as high-order surface modeling and texturing. An extensive survey carried out by \citeauthor{quad_mesh_survey2013} \cite{quad_mesh_survey2013} explains the advantages, generation and processing of a quad-mesh. In addition, many 3D modeling software such as Blender \cite{blender} and Maya \cite{maya} supports designing the 3D quad-mesh based models.
\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth]{images/bunny_mesh.pdf}
	\caption{Depiction of a triangulated mesh, a quad-mesh and a hybrid mesh of the Stanford bunny 3D model. Source: \cite{mcneel_2021,bunny_hybrid_image}.}
	\label{bunnymeshes}
\end{figure}

The \cref{bunnymeshes} illustrates the Stanford bunny model with three different types of meshing formats, namely triangulated mesh (left), quad-mesh (middle) and a hybrid mesh (right). Additionally, there exist complex meshes, as depicted in the \cref{school_model,bridge_model}, that cannot be processed easily due to diverse sizes of triangles. Fusing tiny triangles of the mesh to form comparatively regular-sized polygons throughout the mesh using a fusing algorithm shall simplify the process of sampling viewpoints for complex  meshes. This motivates the thesis to extend the developed inspection planner to support polygons. 

The new code base provides an advantage for extending the inspection planner to support polygons by the formulation of the constraint matrix dynamically and flexible code design that facilitates easy changes to the existing planner. Minor changes are necessary to support the polygons. The AGP solver is created for every facet in the mesh as explained in the \cref{SIP:VPS,Imp:VPSampling}. The solver instance requires number of variables and number of constraints to sample a viewpoint. The number of variables is by default set to three as the viewpoint to be sampled is in 3D space. The number of constraints depends on planar constraints, the four \acrshort{fov} constraints and, if needed, other constraints. The planar constraints depend on the vertices of the triangle and the corresponding hyperplanes normal formed by the edges of the triangle. Therefore, out of four planar constraints, three of them depend on the number of vertices, that translates to three for a triangle. 

To extend the planner to polygons, the creation process of the solver for every facet is modified to check the polygon instance for the number of vertices. Furthermore, the initialization method (\cref{SIP:Structure,Imp:VPSampling}) that computes properties of the facet such as area, area normal vector, hyperplane normal vectors and centroid are modified accordingly to accommodate the polygons. In addition, the new code base already formulates constraint matrix dynamically, the only change required is in the planar constraint initialization. To this end, the initialization expects only triangle facet as an input for constraint construction. Therefore, the planar constraint initialization method is modified to consider the number of vertices of the polygon for constructing the planar constraints as depicted in the \cref{extensionconstarintmatrix}.

\begin{equation}
	\label{extensionconstarintmatrix}
	\begin{aligned}
		\begin{bmatrix}
			\infty\\
			\infty\\
			\vdots\\
			\infty\\
			d_{\text{max}}\\
			\infty\\
			\infty\\
			\infty\\
			\infty
		\end{bmatrix}
		\geq
		\begin{bmatrix}
			\hat{n}_1^T\\
			\hat{n}_2^T\\
			\vdots\\
			\hat{n}_n^T\\
			\hat{a_N}^T\\
			\hat{n}_{\text{right}}^T\\
			\hat{n}_{\text{left}}^T\\
			\hat{n}_{\text{lower}}^T\\
			\hat{n}_{\text{upper}}^T
		\end{bmatrix} \cdot \vg^k
		\geq
		\begin{bmatrix}
			\hat{n}_1^T \cdot \vx_1\\
			\hat{n}_2^T \cdot \vx_2\\
			\vdots\\
			\hat{n}_n^T \cdot \vx_n\\
			d_{\text{min}} + \hat{a_N}^T \cdot \vm\\
			\hat{n}_{\text{right}}^T \cdot \vm\\
			\hat{n}_{\text{left}}^T \cdot \vm\\
			\hat{n}_{\text{lower}}^T \cdot x_{\text{lower}}^{\text{cam}}\\
			\hat{n}_{\text{upper}}^T \cdot x_{\text{upper}}^{\text{cam}}
		\end{bmatrix}
	\end{aligned}
\end{equation}
\clearpage  

In addition, the visibility check method is adapted to the new change by checking whether all the hyperplane normal vectors of the polygon are directed towards the sampled viewpoint position as well as whether all the vertices are inside the camera frustum. The planner node is therefore capable of accommodating polygons that includes triangles, quadrilaterals and n-gons. This is a first attempt of the thesis to support polygons. The observations of the polygon extension are explained in the \cref{results:polygons}.

In special cases, the triangulated mesh contains a plethora of triangles in the mesh with irregular facet sizes as depicted in the \cref{school_model}. Some of the meshes have very tiny triangles to construct the model as shown in the \cref{bridge_model}. In either of the scenarios, the mesh-based inspection planner fails to sample viewpoints satisfying all the conditions for every triangle in the mesh. More number of triangles causes longer processing time and requires large memory space. Tiny triangles in the mesh cause ambiguity in the viewpoint's distance from the triangle. A mix of large number of triangles with irregular size shall fail to sample viewpoints satisfying a particular resolution criterion.

\begin{figure}[h]
	\centering
	\subfloat[Raw model.]{\includegraphics[width=\linewidth]{images/school_raw.pdf}}
	\\
	\subfloat[Model showing triangles.]{\includegraphics[width=\linewidth]{images/school_triangles.pdf}}
	\caption{3D model of a school. Source: \cite{isprs-archives-XLIII-B1-2021-157-2021}.}
	\label{school_model}
\end{figure}

\begin{figure}[h]
	\centering
	\subfloat[Raw model.]{\includegraphics[width=\linewidth]{images/bridge_raw.pdf}}
	\\
	\subfloat[Model showing triangles.]{\includegraphics[width=\linewidth]{images/bridge_triangles.pdf}}
	\caption{3D model of a pier of a bridge. Source: \cite{isprs-archives-XLIII-B1-2021-157-2021}.}
	\label{bridge_model}
\end{figure}
   
To plan a trajectory for the mesh models depicted in the \cref{school_model,bridge_model} is hard and almost impossible. A reduction and simplification of the mesh is necessary to make it usable with the inspection planner. A re-meshing software such as ACVD developed by \citeauthor{acvd1} \cite{acvd1,acvd2} can be used to reduce the number of facets in the mesh. The need for a separate sub-node in the planner node that pre-process the mesh input by reducing the number of triangles in the mesh and regularizing the size of the triangle is crucial. This drives the work of the thesis to develop a region-growing algorithm that fuses triangles into polygons.  
\clearpage
\subsection*{Simple Region-Growing Algorithm}
\label{Imp:SRGA}
To facilitate the pre-processing of a mesh as a part of the planner node, the thesis implements a basic region-growing sub-node. A group of triangles are joined together to form a polygon. The closest neighbours to a triangle of interest are the triangles that share the half-edge. Therefore, the region growing algorithm makes use of the closest neighbours and fuse the triangles that are eligible. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{images/region_growing.pdf}
	\caption{The process of neighbours triangle-fusing. The orange depicts the connected neighbours and the green represents the triangle of interest (left) as well as the resulting polygon (right).}
	\label{region_growing}
\end{figure}

The \cref{region_growing} shows the process of triangle-fusing process and the obtained end polygon. The triangle in green is the triangle of interest and the triangles in orange are the neighbours. After the fusion, the sub-node provides the vertices of the polygon. In some cases, similar to \cref{region_growing}, the result of the fusion might be a concave polygon. To convert it to a convex polygon, a convex hull algorithm is used. The dotted line in the \cref{region_growing} represents the edge of the polygon after the convexification.  The region-growing sub-node receives the list of triangles from the mesh. The user specifies the triangle of interest and the threshold area for the fused region beyond which the triangles are not fused into the main triangle. At first, the algorithm checks the area normal vector of the main triangle and filters the triangles in the list that are identical to the main triangle. 

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\linewidth]{images/anticlock-clock.pdf}
	\caption{Area normal vector for a polygon based on the arrangement of the vertices. Left: Area normal vector for an anti-clockwise arrangement. Right: Area normal vector for a clockwise arrangement.}
	\label{anticlock-clock}
\end{figure} 

Then, the algorithm searches the triangles that share a half-edge with the main triangle. These triangles are, thus, termed as the neighbours as explained before. Since, a threshold value for the area is specified, the algorithm fuses the triangles until the area of the whole polygon does not exceed the threshold. The common vertices to the fused triangles are grouped together that are responsible to construct the polygon. For a closed 3D model, the facet normal vectors are pointing outwards. The outward normal vectors are a direct result of arranging the vertices in the anti-clockwise direction as shown in the \cref{anticlock-clock}. Similarly, for an inward normal vectors, the vertices are arranged in clockwise direction. In 3D space, arranging the vertices in anti-clockwise or clockwise direction is not straightforward unless a reference point or origin ($\vo_{3D}$) on the polygon plane and the reference normal vector are known. In this thesis, the centroid of the polygon and the main triangle's area normal vector are chosen to be a reference point and the normal vector, respectively. Further, an arbitrary $XY^{'}$-plane is chosen to project the 3D vertices to 2D space. The \cref{proj3dto2d} formulate the projection from 3D to 2D.

\begin{equation*}
	\begin{aligned}
		d_{vert} = v_{\text{3D}} - \vo_{\text{3D}} \,, \quad \text{where $v_{\text{3D}}$ is a vertex.} 
	\end{aligned}
\end{equation*}

\begin{equation*}
	\begin{aligned}
		\vctproj[v]{d_{\text{vert}}} = d_{\text{vert}} - (d_{\text{vert}} \cdot \hat{n}) \, . \, \hat{n}
	\end{aligned}
\end{equation*}

\begin{equation*}
	\begin{aligned}
		\hat{x}^{'} = \hat{n} \begin{bmatrix}
			0 & -1 & 0\\
			1 & 0 & 0\\
			0 & 0 & 1\\
		\end{bmatrix} \quad , \hat{y}^{'} = \hat{n} \times \hat{x}^{'}
	\end{aligned}
\end{equation*}

\begin{equation}
	\label{proj3dto2d}
	\begin{aligned}
		{v}_{2D} = [\vctproj[v]{d_{vert}} \cdot \hat{x}^{'}, \vctproj[v]{d_{vert}} \cdot \hat{y}^{'}]
	\end{aligned}
\end{equation}

Furthermore, the angle to individual 2D vertices are calculated as formulated by the \cref{atanequation}. The vertices are arranged in the ascending order of the angles to obtain an anti-clockwise arrangement or the facet normal vector directing outwards.

\begin{equation}
	\label{atanequation}
	\begin{aligned}
		\text{angle},\, \gamma_{i} = \tan^{-1} \left(\frac{v_{iy} - \vo_{y}}{v_{ix} - \vo_{x}}\right) \, , 
	\end{aligned}
\end{equation} 

The initial phase of the region-growing algorithm is complete. In addition, the convex hull algorithm can be integrated to obtain only convex polygons. Due to the limited time, the implementation is halted at this stage. Although, the region-growing sub-node is not yet ready to be integrated into the planner node to pre-process the mesh, the full algorithm is expected to be developed in future work.