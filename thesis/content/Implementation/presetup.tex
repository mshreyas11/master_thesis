\section{Preliminary Setup}
\label{Imp:presetup}
This section shall present the system specifications on which the implementation and testing were carried out, as well as the subsequent steps taken to adapt the native SIP distribution for testing.

\subsubsection*{System Specification}
\label{Imp:presetup:ss}
\begin{itemize}
	\setlength{\itemsep}{1pt}
	\item CPU: AMD Ryzen 7 3800x8-core processor x 16
	\item GPU: NVIDIA GeForce RTX 2060
	\item RAM: 16 GB
	\item OS: Ubuntu 18.04 64-bit
	\item Environment: ROS Melodic
	\item Programming language: C/C++
\end{itemize}

\subsection*{Initial Actions}
Blender software \cite{blender}, a 3D modeling toolkit, is employed in generating the custom 3D-mesh for the thesis. Additionally, the Meshlab tool \cite{meshlab}, a popular 3D mesh editing software, is used in this thesis to support the generation of custom 3D-meshes. A simple rectangular building replicating the dimensions of a real warehouse is designed using Blender. Adding to the set of meshes at the disposal, a 3D mesh of a generic wall and the rectangular building with different tessellations are designed. A suitable non-repetitive texture is applied onto the meshes not only to provide a realistic look when used in simulation environment but for the purpose of possible 3D reconstruction which demands a unique texture pattern on the surface of the inspection specimen.

\begin{figure}[h]
	\centering
	\subfloat[Rectangular building.]{
		\includegraphics[height=4cm, width=0.49\linewidth]{images/robdekon.png}
		\label{Imp:robdekon_model}
	}
	\subfloat[Simple wall.]{
		\includegraphics[height=4cm, width=0.49\linewidth]{images/wall.png}
		\label{Imp:simple_wall_model}
	}
	\caption{Meshes designed using Blender.}
	\label{Imp:Blendermeshimage}
\end{figure}

From \cref{rw:simulation_usage}, it is clear that multiple simulators are available to test the implemented work, since the environment that SIP already supports is the ROS platform. A light-weight simulator that supports ROS and provides numerous functionalities to control the UAV is the best simulator for the thesis. Comparing all UAV frameworks, PX4-Autopilot \cite{PX4} is found to be the best choice to pair up with the inspection planner. PX4 supports QGroundControl (QGC) \cite{don_gagne_2019_3351707}, a ground control station software that can control the way-point flights on PX4 stack and provides options to feed a tour file. QGC is available as a stand-alone application.

To this end, SIP generates a tour file and publishes visualization topics suitable for qualitative inspection using rviz. It improves the adaptability and paves way for further extensions, a small sub-block is implemented that collects the final viewpoint trajectory and publish it for further usage. However, SIP does not store the final trajectory in a standard ROS message format. The sub-block converts the native viewpoint data into geometry\_msgs/PoseArray\footnote{\href{https://docs.ros.org/en/noetic/api/geometry_msgs/html/msg/PoseArray.html}{https://docs.ros.org/en/noetic/api/geometry\_msgs/html/msg/PoseArray.html}} message-type. In addition, rejected triangles, if any, shall be marked red in the rviz tool as a new visualization feature.

% Use the tour.csv data and convert to px4 format with SIP utils
In this thesis, the rectangular building model is used as a standard mesh for the implementation procedures. An inspection trajectory is generated and stored inside a tour.csv file, which is then converted into QGC mission mode format. Moreover, the viewpoint locations are converted into geographic coordinates in the WGS84 coordinate system. The \cref{presetup:qgcconvert} shows the conversion process.

\begin{figure}[h]
	\centering
	\subfloat{\includegraphics[width=0.8\linewidth]{images/qgcformatconvertdiagram.pdf}}\\
	\caption{Block diagram depicting conversion of tour file into QGroundControl format.}
	\label{presetup:qgcconvert}
\end{figure}

There are many coordinate frames that QGC allows the user to work with. In the thesis, the FRAME\_GLOBAL ($encoding \rightarrow 0$) is used. The command field in the format refers to different commands available in mission protocol such as MAV\_CMD\_NAV\_WAYPOINT, MAV\_CMD\_NAV\_LAND and MAV\_CMD\_NAV\_LOITER\_TIME. Based on the value assigned to this field, the parameter placeholder meaning changes. In this thesis, the waypoint mission or MAV\_CMD\_NAV\_WAYPOINT ($encoding \rightarrow 16$) is selected. The four parameter fields are translated as follows:
\begin{itemize}
	\setlength{\itemsep}{1pt}
	\item Param1 $\rightarrow$ Hold time (s) at the waypoint.
	\item Param2 $\rightarrow$ Acceptance radius (m).
	\item Param3 $\rightarrow$ Pass through flag, if within acceptance radius.
	\item Param4 $\rightarrow$ Yaw angle (rad)
\end{itemize}

Other fields, namely: latitude, longitude and altitude follows WGS84 format. An example line in the QGC waypoint file is shown in the \cref{presetup:qgcformat}. 

\begin{figure}[h]
	\centering
	\begin{verbnobox}[\small]
		QGC WPL <version>
		<index> <current wp> <coord frame> <command> <param1> <param2> <param3> <param4> 
		... <lat> <lon> <alt> <autocontinue>
	\end{verbnobox}\\ \\
	\begin{verbnobox}[\small]
		QGC WPL 110
		. . .
		i    0    0    16    0.50    0    0    0.256    8.426540    49.015769    30    1
		.
		.
	\end{verbnobox}
	\caption{QGC content format\footnotemark and corresponding example at $i^{th}$ line.}
	\label{presetup:qgcformat}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[height=5cm]{images/iris_fpv_model.jpg}
	\caption{3D Robotics Iris UAV gazebo model.}
	\label{presetup:iris_model}
\end{figure}
 
In the next step, the PX4 stack is started using the mavros \cite{mavros_2014} wrapper application that makes use of mavlink \cite{mavlink_2009} protocol to communicate with PX4 autopilot and eventually controls the \acrshort{uav} on the Gazebo simulator. The 3DR Iris\footnotetext{\href{https://mavlink.io/en/file_formats/}{https://mavlink.io/en/file\_formats/}}\footnote{\href{https://www.arducopter.co.uk/iris-quadcopter-uav.html}{https://www.arducopter.co.uk/iris-quadcopter-uav.html}} UAV model is chosen as a virtual twin vehicle in the simulation environment as shown in the \cref{presetup:iris_model}. An additional FPV camera is fixed in front of the body as an inspection sensor.

Using the \acrshort{qgc} tool, the \acrshort{uav} is armed and ready for flight. A mission mode is then started by loading the waypoint file converted from the tour file in the previous step. The UAV flies around the region of interest and passes through the waypoints with the corresponding attitude. Although the QGC waypoint mission mode is good enough for controlling the UAV, the cumbersome process of setting up all the sub-elements individually is not an option in the long run and does not meet the goal of the desired automated pipeline. It is also found that the UAV is not necessarily reaching the exact location but grazed around the acceptance radius and skipping nearby waypoints that are too close.

To solve two of these issues, an extensive control of the \acrshort{uav} is necessary. PX4 provides an option of controlling the UAV using off-board mode. The user can develop an entirely new node to control the UAV with exhaustive options. \cref{Imp:offbcntl} shall explain the implementation of the off-board control node as a part of the pipeline. 
